/*
 * Copyright 2019 Johannes Strelka-Petz <johannes@strelka.at>
 *
 */

/* This file is part of oskar_esp32.
 *
 * oskar_esp32 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * oskar_esp32 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with oskar_esp32.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

uint16_t varaiable1=0;

// Erich Schmids 8 Keys Braille
//const static uint16_t chord_id_keycode[256][4]={
uint16_t chord_id_keycode[256][4]={
  #include "undefkeylayouts.h"
  #define LAYOUT_GERMAN
  #define KEY_LT 0x64
  #include "keylayouts.h"
  //{char, char_modifier, deadkey, deadkey_modifier}
  {0,0,0,0}, // 0
  {KEY_A,0,0,0}, // 'a', 0x61, 1 0x01 dots-1
  {KEY_COMMA,0,0,0}, //',', 0x2c, 2 0x02 dots-2
  {KEY_B,0,0,0}, // 'b', 0x62, 3 0x03 dots-12
  {KEY_PERIOD,0,0,0}, //'.', 0x2e, 4 0x04 dots-3
  {KEY_K,0,0,0}, // 'k', 0x6b, 5 0x05 dots-13
  {KEY_COMMA, MODIFIERKEY_SHIFT,0,0}, // ';', 0x3b, 6 4+2 0x06 dots-23
  {KEY_L,0,0,0}, // 'l', 0x6c, 7 0x07 dots-123
  {KEY_2, MODIFIERKEY_SHIFT,0,0}, // '"', 0x22, 8 dots-4
  {KEY_C,0,0,0}, // 'c', 0x63, 9 8+1 0x09 dots-14
  {KEY_I,0,0,0}, // 'i', 0x69, 10 8+2 0x0a dots-24
  {KEY_F,0,0,0}, // 'f', 0x66, 11 8+2+1 0x0b dots-124
  {KEY_LT,MODIFIERKEY_RIGHT_ALT,0,0}, // '|', 0x007c, 12 8+4 0x0c dots-34 '|'
  {KEY_M,0,0,0}, // 'm', 0x6D, 13 8+4+1 0x0d dots-134
  {KEY_S,0,0,0}, // 's', 0x73, 14 8+4+2 0x0e dots-234
  {KEY_P,0,0,0}, // 'p', 0x70, 15 8+4+2+1 0x0f dots-1234
  {KEY_1,MODIFIERKEY_SHIFT,0,0}, // '!', 0x21, 16  0x10 dots-5
  {KEY_E,0,0,0}, // 'e', 0x65, 17 16+1 0x11 dots-15
  {KEY_PERIOD,MODIFIERKEY_SHIFT,0,0}, // ':', 0x3A, 18 16+2 0x12 dots-25
  {KEY_H,0,0,0}, // 'h', 0x68, 19 16+2+1 0x13 dots-125
  {KEY_RIGHT_BRACE,MODIFIERKEY_SHIFT,0,0}, // '*', 0x2A, 20 16+4 0x14 dots-35 '*'
  {KEY_O,0,0,0}, // 'o', 0x6F, 21 16+4+1 0x15 dots-135
  {KEY_RIGHT_BRACE,0,0,0}, // '+', 0x2B, 22 16+4+2 0x16 dots-235
  {KEY_R,0,0,0}, // 'r', 0x72, 23 16+4+2+1 0x17 dots-1235
  {KEY_LT,MODIFIERKEY_SHIFT,0,0}, // '>', 0x3E, 24 16+8 0x18 dots-45 
  {KEY_D,0,0,0}, // 'd', 0x64, 25 16+8+1 0x19 dots-145
  {KEY_J,0,0,0}, // 'j', 0x6A, 26 16+8+2 0x1a dots-245
  {KEY_G,0,0,0}, // 'g', 0x67, 27 16+8+2+1 0x1b dots-1245
  {KEY_SPACE,0,DEADKEY_ACUTE_ACCENT,MODIFIERKEY_SHIFT}, // '`', 0x60, 28 16+8+4 0x1c dots-345 '`'
  {KEY_N,0,0,0}, // 'n', 0x6E, 29 16+8+4+1 0x1d dots-1345
  {KEY_T,0,0,0}, // 't', 0x74, 30 16+8+4+2 0x1e dots-2345
  {KEY_Q,0,0,0}, // 'q', 0x71, 31 16+8+4+2+1 0x1f dots-12345
  {KEY_BACKSLASH,MODIFIERKEY_SHIFT,0,0}, // ''', 0x, 32 0x20 dots-6 apostroph
  {KEY_1,0,0,0}, // '1', 0x31, 33 32+1 0x21 dots-16 'au'
  {KEY_MINUS,MODIFIERKEY_SHIFT,0,0}, // '?', 0x3F, 34 32+2 0x22 dots-26
  {KEY_2,0,0,0}, // '2', 0x32, 35 32+2+1 0x23 dots-126 'eu'
  {KEY_SLASH,0,0,0}, // '-', 0x2D, 36 32+4 0x24 dots-36
  {KEY_U,0,0,0}, // 'u', 0x75, 37 32+4+1 0x25 dots-136
  {KEY_8,MODIFIERKEY_SHIFT,0,0}, // '(', 0x28, 38 32+4+2 0x26 dots-236 
  {KEY_V,0,0,0}, // 'v', 0x76, 39 32+4+2+1 0x27 dots-1236
  {KEY_4,MODIFIERKEY_SHIFT,0,0}, // '$', 0x24, 40 32+8 0x28 dots-46
  {KEY_3,0,0,0}, // '3', 0x33, 41 32+8+1 0x29 dots-146
  {KEY_9,0,0,0}, // '9', 0x39, 42 32+8+2 0x2a dots-246
  {KEY_6,0,0,0}, // '6', 0x36, 43 32+8+2+1 0x2b dots-1246 
  {KEY_0,0,0,0}, // '0', 0x30, 44 32+8+4 0x2c dots-346
  {KEY_X,0,0,0}, // 'x', 0x78, 45 32+8+4+1 0x2d dots-1346
  {KEY_RIGHT_BRACE,MODIFIERKEY_RIGHT_ALT,0,0}, // '~', 0x7E, 46 32+8+4+2 0x2e dots-2346
  {KEY_6,MODIFIERKEY_SHIFT,0,0}, // '&', 0x26, 47 32+8+4+2+1 0x2f dots-12346
  {KEY_LT,0,0,0}, // '<', 0x3C, 48 32+16 0x30 dots-56
  {KEY_5,0,0,0}, // '5', 0x35, 49 32+16+1 0x31 dots-156
  {KEY_7,MODIFIERKEY_SHIFT,0,0}, // '/', 0x2F, 50 32+16+2 0x32 dots-256
  {KEY_8,0,0,0}, // '8', 0x38, 51 32+16+2+1 0x33 dots-1256
  {KEY_9,MODIFIERKEY_SHIFT,0,0}, // ')', 0x29, 52 32+16+4 0x34 dots-356
  {KEY_Y,0,0,0}, // 'z', 0x7A, 53 32+16+4+1 0x35 dots-1356
  {KEY_0,MODIFIERKEY_SHIFT,0,0}, // '=', 0x3D, 54 32+16+4+2 0x36 dots-2356
  {KEY_7,MODIFIERKEY_RIGHT_ALT,0,0}, // '{', 0x7B, 55 32+16+4+2+1 0x37 dots-12356
  {KEY_SLASH,MODIFIERKEY_SHIFT,0,0}, // '_', 0x5F, 56 32+16+8 0x38 dots-456
  {KEY_4,0,0}, // '4', 0x34, 57 32+16+8+1 0x39 dots-1456
  {KEY_W,0,0,0}, // 'w', 0x77, 58 32+16+8+2 0x3a dots-2456
  {KEY_7,0,0,0}, // '7', 0x37, 59 32+16+8+2+1 0x3b dots-12456
  {KEY_BACKSLASH,0,0,0}, // '#', 0x23, 60 32+16+8+4 0x3c dots-3456
  {KEY_Z,0,0,0}, // 'y', 0x79, 61 32+16+8+4+1 0x3d dots-13456
  {KEY_0,MODIFIERKEY_RIGHT_ALT,0,0}, // '}', 0x7D, 62 32+16+8+4+2 0x3e dots-23456
  {KEY_5,MODIFIERKEY_SHIFT,0,0}, // '%', 0x25, 63 32+16+8+4+2+1 0x3f dots-123456
  {KEY_BACKSPACE,0,0,0}, // 'bs', 0x, 64 dots-7 backspace
  {KEY_A,MODIFIERKEY_SHIFT,0,0}, // 'A', 0x41, 65 dots-17
  {0,0,0,0}, // 0, 66 dots-27
  {KEY_B,MODIFIERKEY_SHIFT,0,0}, // 'B', 0x42, 67 dots-127
  {0,0,0,0}, // 0, 68 dots-37
  {KEY_K,MODIFIERKEY_SHIFT,0,0}, // 'K', 0x4B, 69 dots-137
  {0,0,0,0}, // 0, 70 dots-237
  {KEY_L,MODIFIERKEY_SHIFT,0,0}, // 'L', 0x4C, 71 dots-1237
  {KEY_BACKSLASH,MODIFIERKEY_RIGHT_ALT,0,0}, // '’' 0x2019, 2019, 72 dots-47 '’'
  {KEY_C,MODIFIERKEY_SHIFT,0,0}, // 'C', 0x43, 73 dots-147
  {KEY_I,MODIFIERKEY_SHIFT,0,0}, // 'I', 0x49, 74 dots-247
  {KEY_F,MODIFIERKEY_SHIFT,0,0}, // 'F', 0x46, 75 dots-1247
  {KEY_MINUS,MODIFIERKEY_RIGHT_ALT,0,0}, // '\\', 0x5C, 76 dots-347
  {KEY_M,MODIFIERKEY_SHIFT,0,0}, // 'M', 0x4D, 77 dots-1347
  {KEY_S,MODIFIERKEY_SHIFT,0,0}, // 'S', 0x53, 78 dots-2347
  {KEY_P,MODIFIERKEY_SHIFT,0,0}, // 'P', 0x50, 79 dots-12347
  {0,0,0,0}, // 0, 80 dots-57
  {KEY_E,MODIFIERKEY_SHIFT,0,0}, // 'E', 0x45, 81 dots-157
  {0,0,0,0}, // 0, 82 dots-257
  {KEY_H,MODIFIERKEY_SHIFT,0,0}, // 'H', 0x48, 83 dots-1257
  {0,0,0,0}, // 0, 84 dots-357
  {KEY_O,MODIFIERKEY_SHIFT,0,0}, // 'O', 0x4F, 85 dots-1357
  {0,0,0,0}, // 0, 86 dots-2357
  {KEY_R,MODIFIERKEY_SHIFT,0,0}, // 'R', 0x52, 87 dots-12357
  {KEY_E,MODIFIERKEY_RIGHT_ALT,0,0}, // '€', 20AC, 88 dots-457 '€'
  {KEY_D,MODIFIERKEY_SHIFT,0,0}, // 'D', 0x44, 89 dots-1457
  {KEY_J,MODIFIERKEY_SHIFT,0,0}, // 'J', 0x4A, 90 dots-2457
  {KEY_G,MODIFIERKEY_SHIFT,0,0}, // 'G', 0x47, 91 dots-12457
  {KEY_Q,MODIFIERKEY_RIGHT_ALT,0,0}, // '@', 0x40, 92 dots-3457
  {KEY_N,MODIFIERKEY_SHIFT,0,0}, // 'N', 0x4E, 93 dots-13457
  {KEY_T,MODIFIERKEY_SHIFT,0,0}, // 'T', 0x54, 94 dots-23457
  {KEY_Q,MODIFIERKEY_SHIFT,0,0}, // 'Q', 0x51, 95 dots-123457
  {0,0,0,0}, // 0, 96 dots-67
  {KEY_A,MODIFIERKEY_SHIFT,DEADKEY_CIRCUMFLEX,0}, // 'Â', 0xC2, 97 dots-167 'Â'
  {0,0,0,0}, // 0, 98 dots-267
  {KEY_E,MODIFIERKEY_SHIFT,DEADKEY_CIRCUMFLEX,0}, // 'Ê', 0xCA, 99 dots-1267 'Ê'
  {0,0,0,0}, // 0, 100 dots-367
  {KEY_U,MODIFIERKEY_SHIFT,0,0}, // 'U', 0x55, 101 dots-1367
  {0,0,0,0}, // 0, 102 dots-2367
  {KEY_V,MODIFIERKEY_SHIFT,0,0}, // 'V', 0x56, 103 dots-12367
  {0,0,0,0}, // 0, 104 dots-467
  {KEY_I,MODIFIERKEY_SHIFT,DEADKEY_CIRCUMFLEX,0}, // 'Î', 0xCE, 105 dots-1467 'Î'
  {0,0,0,0}, // 0, 106 dots-2467
  {0,0,0,0}, // 0, 107 dots-12467
  {0,0,0,0}, // 0, 108 dots-3467
  {KEY_X,MODIFIERKEY_SHIFT,0,0}, // 'X', 0x58, 109 dots-13467
  {KEY_SPACE,0,DEADKEY_CIRCUMFLEX,0}, // '^', 0x5E, 110 dots-23467 '^'
  {KEY_C,MODIFIERKEY_SHIFT,DEADKEY_ACUTE_ACCENT,MODIFIERKEY_RIGHT_ALT}, // 'Ç', 0xC7, 111 dots-123467 'Ç'
  {KEY_QUOTE,MODIFIERKEY_SHIFT,0,0}, // 'Ä', 0xC4, 112 dots-567 'Ä'
  {KEY_U,MODIFIERKEY_SHIFT,DEADKEY_CIRCUMFLEX,0}, // 'Û', 0xDB, 113 dots-1567 'Û'
  {0,0,0,0}, // 0, 114 dots-2567
  {0,0,0,0}, // 0, 115 dots-12567
  {0,0,0,0}, // 0, 116 dots-3567
  {KEY_Y,MODIFIERKEY_SHIFT,0,0}, // 'Z', 0x5A, 117 dots-13567
  {0,0,0,0}, // 0, 118 dots-23567
  {KEY_8,MODIFIERKEY_RIGHT_ALT,0,0}, // '[', 0x5B, 119 dots-123567 '['
  {0,0,0,0}, // 0, 120 dots-4567
  {KEY_O,MODIFIERKEY_SHIFT,DEADKEY_CIRCUMFLEX,0}, // 'Ô', 0xD4, 121 dots-14567 'Ô'
  {KEY_W,MODIFIERKEY_SHIFT,0,0}, // 'W', 0x57, 122 dots-24567
  {0,0,0,0}, // 0, 123 dots-124567
  {0,0,0,0}, // 0, 124 dots-34567
  {KEY_Z,MODIFIERKEY_SHIFT,0,0}, // 'Y', 0x59, 125 dots-134567
  {KEY_9,MODIFIERKEY_RIGHT_ALT,0,0}, // ']', 0x5D, 126 dots-234567
  {KEY_E,MODIFIERKEY_SHIFT,DEADKEY_ACUTE_ACCENT,0}, // 'É', 0xC9, 127 dots-1234567 'É'
  {KEY_SPACE,0,0,0}, // ' ', 0x, 128 dots-8 space
  {KEY_TAB,0,0,0}, // 'ht', 0x, 129 dots-18 horizontal tab
  {0,0,0,0}, // 0, 130
  //{KEY_2,MODIFIERKEY_RIGHT_ALT,0,0}, // '²', 0xB2,  131 dots-128 '²'
  {KEY_TAB,MODIFIERKEY_SHIFT,0,0}, // '', 0x,  131 dots-128 shift tab
  {0,0,0,0}, // 0, 132
  {KEY_B,MODIFIERKEY_RIGHT_ALT,0,0}, // '“', 201C,  133 dots-138 '“'
  {0,0,0,0}, // 0, 134
  {0,0,0,0}, // 0, 135
  {0,0,0,0}, // 0, 136
  {KEY_3,MODIFIERKEY_RIGHT_ALT,0,0}, // '³', 0xB3,  137 dots-148 '³'
  {0,0,0,0}, // 0, 138
  {0,0,0,0}, // 0, 139
  {0,0,0,0}, // 0, 140
  {0,0,0,0}, // 0, 141
  {0,0,0,0}, // 0, 142
  {0,0,0,0}, // 0, 143
  {KEY_RIGHT,MODIFIERKEY_LEFT_ALT,0,0}, // '', 0x, 144 dots-58 next element, Alt + Right arrow 
  {0,0,0,0}, // 0, 145
  {KEY_LEFT,MODIFIERKEY_LEFT_ALT,0,0}, // '', 0x, 146 dots-258 previous element, Alt + Left arrow
  {0,0,0,0}, // 0, 147
  {KEY_SEMICOLON,MODIFIERKEY_SHIFT,0,0}, // 'Ö', 0xD6,  148 dots-358 'Ö'
  {0,0,0,0}, // 0, 149
  {0,0,0,0}, // 0, 150
  {0,0,0,0}, // 0, 151
  {0,0,0,0}, // 0, 152
  {0,0,0,0}, // 0, 153
  {0,0,0,0}, // 0, 154
  {0,0,0,0}, // 0, 155
  {KEY_QUOTE,0,0,0}, // 'ä', 0xE4,  156 dots-3458 'ä'
  {0,0,0,0}, // 0, 157
  {0,0,0,0}, // 0, 158
  {0,0,0,0}, // 0, 159
  {0,0,0,0}, // 0, 160
  {0,0,0,0}, // 0, 161
  {0,0,0,0}, // 0, 162
  {KEY_V,MODIFIERKEY_RIGHT_ALT,0,0}, // '„', 0x201E,  163 dots-1268 '„'
  {0,0,0,0}, // 0, 164
  {0,0,0,0}, // 0, 165
  {KEY_LEFT_BRACE,MODIFIERKEY_SHIFT,0,0}, // 'Ü', 0xDC,  166 dots-2368 'Ü'
  {0,0,0,0}, // '½', 0xBD,  167 dots-12368 '½'
  {0,0,0,0}, // 0, 168
  {0,0,0,0}, // 0, 169
  {KEY_SEMICOLON}, // 'ö', 0xF6,  170 dots-2468 'ö'
  {0,0,0,0}, // 0, 171
  {KEY_O,0,DEADKEY_ACUTE_ACCENT,MODIFIERKEY_SHIFT}, // 'ò', 0xF2,  172 dots-3468 'ò'
  {0,0,0,0}, // 0, 173
  {0,0,0,0}, // 0, 174
  {0,0,0,0}, // 0, 175
  {0,0,0,0}, // 0, 176
  {0,0,0,0}, // 0, 177
  {0,0,0,0}, // 0, 178
  {KEY_LEFT_BRACE,0,0,0}, // 'ü', 0xFC,  179 dots-12568 'ü'
  {0,0,0,0}, // 0, 180
  {0,0,0,0}, // 0, 181
  {0,0,0,0}, // 0, 182
  {KEY_A,0,DEADKEY_ACUTE_ACCENT,MODIFIERKEY_SHIFT}, // 'à', 0xE0,  183 dots-123568 'à'
  {KEY_TILDE,MODIFIERKEY_SHIFT,0,0}, // '°', 0xB0,  184 dots-4568 '°'
  {0,0,0,0}, // 0, 185
  {0,0,0,0}, // 0, 186
  {0,0,0,0}, // 0, 187
  {KEY_MINUS,0,0,0}, // 'ß', 0xDF,  188 dots-34568 'ß'
  {0,0,0,0}, // 0, 189
  {KEY_U,0,DEADKEY_ACUTE_ACCENT,MODIFIERKEY_SHIFT}, // 'ù', 0xF9,  190 dots-234568 'ù'
  {KEY_E,0,DEADKEY_ACUTE_ACCENT,0}, // 'é', 0xE9,  191 dots-1234568 'é'
  {KEY_ENTER,0,0,0}, // 'cr', 0x, 192 dots 78 carriage return
  {0,0,0,0}, // 0, 193
  {0,0,0,0}, // 0, 194
  {0,0,0,0}, // 0, 195
  {0,0,0,0}, // 0, 196
  {0,0,0,0}, // 0, 197
  {0,0,0,0}, // 0, 198
  {0,0,0,0}, // 0, 199
  {0,0,0,0}, // 0, 200
  {0,0,0,0}, // 0, 201
  {0,0,0,0}, // 0, 202
  {0,0,0,0}, // 0, 203
  {0,0,0,0}, // 0, 204
  {0,0,0,0}, // 0, 205
  {0,0,0,0}, // 0, 206
  {0,0,0,0}, // 0, 207
  {KEY_O,MODIFIERKEY_SHIFT,DEADKEY_ACUTE_ACCENT,MODIFIERKEY_SHIFT}, // 'Ò', 0xD2,  208 dots-578 'Ò'
  {0,0,0,0}, // 0, 209
  {0,0,0,0}, // 0, 210
  {0,0,0,0}, // 0, 211
  {0,0,0,0}, // 0, 212
  {0,0,0,0}, // 0, 213
  {0,0,0,0}, // 0, 214
  {0,0,0,0}, // 0, 215
  {KEY_Z,MODIFIERKEY_RIGHT_ALT,0,0}, // '»', 0xBB,  216 dots-4578 '»'
  {0,0,0,0}, // 0, 217
  {0,0,0,0}, // 0, 218
  {0,0,0,0}, // 0, 219
  {0,0,0,0}, // 0, 220
  {0,0,0,0}, // 0, 221
  {0,0,0,0}, // 0, 222
  {0,0,0,0}, // 0, 223
  {0,0,0,0}, // 0, 224
  {KEY_A,0,DEADKEY_CIRCUMFLEX,0}, // 'â', 0xE2,  225 dots-1678 'â'
  {0,0,0,0}, // 0, 226
  {KEY_E,0,DEADKEY_CIRCUMFLEX,0}, // 'ê', 0xEA,  227 dots-12678 'ê'
  {0,0,0,0}, // 0, 228
  {0,0,0,0}, // 0, 229
  {KEY_A,MODIFIERKEY_SHIFT,DEADKEY_ACUTE_ACCENT,MODIFIERKEY_SHIFT}, // 'À', 0xC0,  230 dots-23678 'À'
  {0,0,0,0}, // 0, 231
  {0,0,0,0}, // 0, 232
  {KEY_I,0,DEADKEY_CIRCUMFLEX,0}, // 'î', 0xEE,  233 dots-14678 'î'
  {0,0,0,0}, // 0, 234
  {0,0,0,0}, // 0, 235
  {0,0,0,0}, // 0, 236
  {0,0,0,0}, // 0, 237
  {0,0,0,0}, // 0, 238
  {KEY_C,MODIFIERKEY_SHIFT,DEADKEY_ACUTE_ACCENT,MODIFIERKEY_RIGHT_ALT}, // 'ç', 0xE7,  239 dots-1234678 'ç'
  {KEY_X,MODIFIERKEY_RIGHT_ALT,0,0}, // '«', 0xAB,  240 dots-5678 '«'
  {KEY_U,0,DEADKEY_CIRCUMFLEX,0}, // 'û', 0xFB,  241 dots-15678 'û'
  {0,0,0,0}, // 0, 242
  {0,0,0,0}, // 0, 243
  {KEY_U,MODIFIERKEY_SHIFT,DEADKEY_ACUTE_ACCENT,MODIFIERKEY_SHIFT}, // 'Ù', 0xD9,  244 dots-35678 'Ù'
  {0,0,0,0}, // 0, 245
  {0,0,0,0}, // 0, 246
  {0,0,0,0}, // 0, 247
  {0,0,0,0}, // 0, 248
  {KEY_O,0,DEADKEY_CIRCUMFLEX,0}, // 'ô', 0xF4,  249 dots-145678 'ô'
  {0,0,0,0}, // 0, 250
  {0,0,0,0}, // 0, 251
  {KEY_S,MODIFIERKEY_RIGHT_ALT | MODIFIERKEY_SHIFT,0,0}, // 'ẞ',  0x1E9E, 252 dots-345678 'ẞ'
  {0,0,0,0}, // 253
  {0,0,0,0}, // 254
  {0,0,0,0}, // 255
};
