/*
 * Copyright 2019 Johannes Strelka-Petz <johannes@strelka.at>
 * 
 * Heavily based on Paul Stoffregens usb_api.cpp from Teensyduino
 * http://www.pjrc.com/teensy/teensyduino.html
 * Copyright (c) 2008 PJRC.COM, LLC
 * THANK YOU VERY MUCH FOR THIS EFFORT ON KEYBOARD + LAYOUTS!
 *
 */

/* This file is part of oskar_esp32.
 *
 * oskar_esp32 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * oskar_esp32 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with oskar_esp32.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
 
#ifndef _BRAILLE_H_
#define _BRAILLE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern uint16_t chord_id_keycode[][4];  
						
#ifdef __cplusplus
}
#endif

#endif

