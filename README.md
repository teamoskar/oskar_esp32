# oskar_esp32
oskar_esp32 is the firmware for Oskar, based on the microcontroller ESP32.
Oskar is a open-source, mobile braillekeyboard.
The arrangement of 8 keys in a braille cell block allows Oskar to be controlled without a supporting surface.

# Build
Please follow the step-by-step instructions of Espressif's ESP-IDF manual to setup the build infrastructure.

This Version builds with esp-idf v3.3. [esp-idf v3.3 Setup Toolchain](https://readthedocs.com/projects/espressif-esp-idf/downloads/pdf/v3.3/)

1. ESP-IDF Get Started
2. Install prerequisites
3. Get ESP-IDF

---
     ~$ mkdir ~/esp

     ~$ cd ~/esp

     ~/esp$ git clone --recursive https://github.com/espressif/esp-idf.git

---

4. Checkout version v3.3

---
     ~/esp$ cd ~/esp/esp-idf
   
     ~/esp/esp-idf$ git checkout v3.3

     ~/esp/esp-idf$ git submodule update --init --recursive

---

5. Set up the tools

---
      ~/esp/esp-idf$ export IDF_PATH="$HOME/esp/esp-idf"
     
      ~/esp/esp-idf$ python -m pip install --user -r $IDF_PATH/requirements.txt

---      

Download https://dl.espressif.com/dl/xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz

---     
      ~/esp/esp-idf$ cd ..
     
      ~/esp$ tar -xzf ~/Downloads/xtensa-esp32-elf-linux64-1.22.0-80-g6c4433a-5.2.0.tar.gz
     
      ~/esp$ export PATH="$HOME/esp/xtensa-esp32-elf/bin:$PATH"
     
---     

enhance ~/.profile

---      
       export PATH="$HOME/esp/xtensa-esp32-elf/bin:$PATH"

       export IDF_PATH="$HOME/esp/esp-idf"

---

6. Clone oskar_esp32

---
      ~/esp$ git clone https://gitlab.com/teamoskar/oskar_esp32.git

---

7. Connect oskar

Now connect your ESP32 board to the computer and check under what serial port the board is visible.

---
      ~/esp$ sudo dmesg

      [ 8474.673211] usb 1-1: ch341-uart converter now attached to ttyUSB0

      ~/esp$ ls /dev/ttyUSB0

      /dev/ttyUSB0

---

8. Configure

---
      ~/esp$ cd oskar_esp32/

      ~/esp/oskar_esp32$ make menuconfig #nothing to do here, answer questions with default answer

---

9. Compile and flash

---
      ~/esp/oskar_esp32$ make all

      ~/esp/oskar_esp32$ make V=1 #with debug info

      ~/esp/oskar_esp32$ make flash

---

10. Monitor

After a successful setup, you should be able to build the oskar_esp firmware by executing 'make monitor'.

---
      ~/esp/oskar_esp32$ make monitor
---

# Contact
[https://oskar.ddns.mobi](https://oskar.ddns.mobi)
Johannes Strelka-Petz <johannes_at_oskar.ddns.mobi>

# Credits and many thanks to
[AsTeRICS Foundation](https://www.asterics-foundation.org/) and special thanks to Benjamin Aigner.

oskar_esp32 is heavily based on AsTeRICS Foundations [ESP32 Mouse/Keyboard for BLE HID](https://github.com/asterics/esp32_mouse_keyboard)