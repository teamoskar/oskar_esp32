/*
 * Copyright 2019 Johannes Strelka-Petz <johannes@strelka.at>
 * 
 * Copyright 2017 Benjamin Aigner <beni@asterics-foundation.org>
 *
 */

/* This file is part of oskar_esp32.
 *
 * oskar_esp32 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * oskar_esp32 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with oskar_esp32.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_bt.h"
#include "driver/gpio.h"

#include "config.h"
#include "HID_kbdmousejoystick.h"

#include "esp_gap_ble_api.h"
//#include "esp_hidd_prf_api.h"
/*#include "esp_bt_defs.h"
#include "esp_gatts_api.h"
#include "esp_gatt_defs.h"
#include "esp_bt_main.h"
#include "esp_bt_device.h"*/
#include "driver/gpio.h"
//#include "driver/uart.h"
//#include "hid_dev.h"
#include "keyboard.h"

/** demo mouse speed */
#define MOUSE_SPEED 30
#define MAX_CMDLEN  100

#define EXT_UART_TAG "EXT_UART"
#define CONSOLE_UART_TAG "CONSOLE_UART"
#define OSKAR_TAG "OSKAR"

static uint8_t keycode_modifier=0;
static uint8_t keycode_deadkey_first=0;
//static joystick_data_t joystick;//currently unused, no joystick implemented
static config_data_t config;
// joystick_command_t joystickCmd;

/* oskar */
#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')

// #define GATTS_TAG "oskar"
#define DEBUG_TAG "debug"

#define braille_1 GPIO_NUM_32
#define braille_2 GPIO_NUM_25
#define braille_3 GPIO_NUM_27
#define braille_4 GPIO_NUM_33
#define braille_5 GPIO_NUM_26
#define braille_6 GPIO_NUM_14
#define braille_7 GPIO_NUM_13
#define braille_8 GPIO_NUM_12

#define joystick_left GPIO_NUM_21
#define joystick_up GPIO_NUM_19
#define joystick_right GPIO_NUM_23
#define joystick_center GPIO_NUM_18
#define joystick_down GPIO_NUM_5 /* blue led */

static gpio_num_t keypins[] = {braille_1, braille_2, braille_3, braille_4, braille_5, braille_6, braille_7, braille_8, joystick_left, joystick_down, joystick_up, joystick_right, joystick_center};
static int keypins_size = (sizeof(keypins)/sizeof(keypins[0]));
static uint16_t chord_id_prev=0;
static bool chord_was_greater_then_chord_prev=false;

static uint8_t sticky_modifier;
//static uint8_t keycode_modifier;
static uint8_t modi=0;
//static uint8_t keycode_deadkey_first;

#include "undefkeylayouts.h"
#define LAYOUT_GERMAN
#include "keylayouts.h"
#define KEY_LT 0x64
#include "braille.h"


void update_config()
{
    nvs_handle my_handle;
    esp_err_t err = nvs_open("config_c", NVS_READWRITE, &my_handle);
    if(err != ESP_OK) ESP_LOGE("MAIN","error opening NVS");
    err = nvs_set_str(my_handle, "btname", config.bt_device_name);
    if(err != ESP_OK) ESP_LOGE("MAIN","error saving NVS - bt name");
    err = nvs_set_u8(my_handle, "locale", config.locale);
    if(err != ESP_OK) ESP_LOGE("MAIN","error saving NVS - locale");
    printf("Committing updates in NVS ... ");
    err = nvs_commit(my_handle);
    printf((err != ESP_OK) ? "Failed!\n" : "Done\n");
    nvs_close(my_handle);
}


void sendKeyCode(uint8_t c, keyboard_action type) {
	keyboard_command_t keyboardCmd;
	keyboardCmd.type = type;
	keyboardCmd.keycode = c + ((uint16_t)keycode_modifier << 8);
	xQueueSend(keyboard_q,(void *)&keyboardCmd, (TickType_t) 0);
}

void sendKey(uint8_t c, keyboard_action type) {
    uint8_t keycode;
	keycode = parse_for_keycode(c,config.locale,&keycode_modifier,&keycode_deadkey_first); //send current byte to parser
	if(keycode == 0) 
	{
		ESP_LOGI(EXT_UART_TAG,"keycode is 0 for 0x%X, skipping to next byte",c);
		return; //if no keycode is found,skip to next byte (might be a 16bit UTF8)
	}
	ESP_LOGI(EXT_UART_TAG,"keycode: %d, modifier: %d, deadkey: %d",keycode,keycode_modifier,keycode_deadkey_first);
	//TODO: do deadkey sequence...
		//if a keycode is found, add to keycodes for HID
	sendKeyCode(keycode, type);
}





#define CMDSTATE_IDLE 0
#define CMDSTATE_GET_RAW 1
#define CMDSTATE_GET_ASCII 2

struct cmdBuf {
    int state;
	int expectedBytes;
	int bufferLength;
	uint8_t buf[MAX_CMDLEN];
} ;

uint8_t uppercase(uint8_t c) 
{
	if ((c>='a') && (c<='z')) return (c-'a'+'A');
	return(c);
}

int get_int(const char * input, int index, int * value) 
{
  int sign=1, result=0, valid=0;

  while (input[index]==' ') index++;   // skip leading spaces
  if (input[index]=='-') { sign=-1; index++;}
  while ((input[index]>='0') && (input[index]<='9'))
  {
	  result= result*10+input[index]-'0';
	  valid=1;
	  index++;
  }
  while (input[index]==' ') index++;  // skip trailing spaces
  if (input[index]==',') index++;     // or a comma
  
  if (valid) { *value = result*sign; return (index);}
  return(0);  
}


void blink_task(void *pvParameter)
{
    // Initialize GPIO pins
    gpio_pad_select_gpio(INDICATOR_LED_PIN);
    gpio_set_direction(INDICATOR_LED_PIN, GPIO_MODE_OUTPUT);
    int blinkTime;
    
    while(1) {
		
		if (HID_kbdmousejoystick_isConnected()) 
			blinkTime=1000;
		else blinkTime=250;
		
		
        /* Blink off (output low) */
        gpio_set_level(INDICATOR_LED_PIN, 0);
        vTaskDelay(blinkTime / portTICK_PERIOD_MS);
        /* Blink on (output high) */
        gpio_set_level(INDICATOR_LED_PIN, 1);
        vTaskDelay(blinkTime / portTICK_PERIOD_MS);
    }
}

/* oskar */
void bt_write(uint16_t keycode, uint8_t modifier)
{
  keyboard_command_t keyboardCmd;
  keyboardCmd.keycode = (0xFF & keycode) | (modifier<<8);
  keyboardCmd.type = PRESS_RELEASE;
  ESP_LOGI(DEBUG_TAG,"bt_write keyboardCmd.keycode: %d", keyboardCmd.keycode);
  xQueueSend(keyboard_q,(void *)&keyboardCmd, (TickType_t) 0);
  
}

void joystick(uint16_t chord_id)
{
  uint16_t j_left=1ULL<<8;
  uint16_t j_down=1ULL<<9;
  uint16_t j_up=1ULL<<10;
  uint16_t j_right=1ULL<<11;
  uint16_t j_center=1ULL<<12;
  // android default keymap
  if (chord_id & j_left){
    //keycode_modifier= (uint8_t) (keycode_modifier | MODIFIERKEY_LEFT_ALT);
    bt_write(KEY_LEFT, keycode_modifier);
  }
  if (chord_id & j_down){
    //keycode_modifier= (uint8_t) (keycode_modifier | MODIFIERKEY_LEFT_ALT);
    bt_write(KEY_DOWN, keycode_modifier);
  }
  if (chord_id & j_up){
    //keycode_modifier= (uint8_t) (keycode_modifier | MODIFIERKEY_LEFT_ALT);
    bt_write(KEY_UP, keycode_modifier);
  }
  if (chord_id & j_right){
    //keycode_modifier= (uint8_t) (keycode_modifier | MODIFIERKEY_LEFT_ALT);
    bt_write(KEY_RIGHT, keycode_modifier);
  }
  if (chord_id & j_center){
    if ((chord_id & 1ULL<<(1-1)) && (chord_id & 1ULL<<(2-1)) && (chord_id & 1ULL<<(5-1))){
      // center, 125, home
       keycode_modifier= (uint8_t) (keycode_modifier | MODIFIERKEY_LEFT_ALT | MODIFIERKEY_LEFT_CTRL);
       bt_write(KEY_H, keycode_modifier);
    }
    else if ((chord_id & 1ULL<<(1-1)) && (chord_id & 1ULL<<(2-1))){
      // center, 12, back
      keycode_modifier= (uint8_t) (keycode_modifier | MODIFIERKEY_LEFT_ALT | MODIFIERKEY_LEFT_CTRL);
      bt_write(KEY_DELETE, keycode_modifier);
    }
    else if ((chord_id & 1ULL<<(1-1)) && (chord_id & 1ULL<<(2-1)) && (chord_id & 1ULL<<(3-1)) && (chord_id & 1ULL<<(5-1))){
      // center, 1235, recent apps
      keycode_modifier= (uint8_t) (keycode_modifier | MODIFIERKEY_LEFT_ALT | MODIFIERKEY_LEFT_CTRL);
      bt_write(KEY_R, keycode_modifier);
    }    
    else {
      // center, click
      keycode_modifier= (uint8_t) MODIFIERKEY_LEFT_ALT;
      bt_write(KEY_ENTER, keycode_modifier);
    }
  }    
}

uint8_t get_sticky_modifier(uint8_t sticky_modifier)
{
  uint8_t keycode_modifier_tmp=sticky_modifier;
  sticky_modifier=0;
  return keycode_modifier_tmp;
}

void set_sticky_modifier(uint16_t chord_id)
{
  switch (chord_id)
    {
    case (1<<(2-1) | 1<<(8-1)):
      {	// Shift 28
	sticky_modifier = (uint8_t) MODIFIERKEY_LEFT_SHIFT;
	break;
      }
    case (1<<(3-1) | 1<<(8-1)):
      {    // Strg 38
	sticky_modifier= (uint8_t) MODIFIERKEY_LEFT_CTRL;
	break;
      }
    case (1<<(2-1) | 1<<(3-1) | 1<<(8-1)):
      {    // Shift+strg 238
	sticky_modifier= (uint8_t) (MODIFIERKEY_LEFT_SHIFT | MODIFIERKEY_LEFT_CTRL) ;
	break;
      }
    // Windows 48
    // JAWS oder Insert 58
    case (1<<(6-1) | 1<<(8-1)):
      {    // Alt 68
	sticky_modifier= (uint8_t) MODIFIERKEY_LEFT_ALT;
	break;
      }
    case (1<<(2-1) | 1<<(6-1) | 1<<(8-1)):
      { // Alt+shift 268 
	sticky_modifier= (uint8_t) (MODIFIERKEY_LEFT_ALT | MODIFIERKEY_LEFT_SHIFT);
	break;
      }
    case (1<<(3-1) | 1<<(6-1) | 1<<(8-1)):
      { // Alt+strg 368
	sticky_modifier= (uint8_t) (MODIFIERKEY_LEFT_ALT | MODIFIERKEY_LEFT_CTRL) ;
	break;
      }
    case (1<<(2-1) | 1<<(3-1) | 1<<(5-1) | 1<<(6-1) | 1<<(8-1)):
      { // Alt+shift+strg 23568 (should be 2368 but ü)
	sticky_modifier= (uint8_t) (MODIFIERKEY_LEFT_ALT | MODIFIERKEY_LEFT_SHIFT | MODIFIERKEY_LEFT_CTRL) ;
	break;
      }
    // Funktionstasten 47 und danach die Buchstaben a-y für F1-F24
    default:
      {
	sticky_modifier=0;
      }
  }
}

uint8_t get_modi(uint16_t chord_id)
{
  if (chord_id == ((1<<(5-1)) | (1<<(7-1)))){ // 57 changes modus
    // keycode_modifier =0; leave keycode_modifier unchanged, use it in the next modus
    switch (modi)
      {
      case 0:
	{
	  modi=modi+1;
	  break;
	}	
      case 1:
	{
	  modi=modi+1;
	  break;
	}
      case 2:
	{
	  modi=0;
	  break;
	}
      default:
	{
	  modi=0;
	  break;
	}
      }
  }
  // modus is not changed
  return modi;
}

uint16_t chord_actual()
{
  uint16_t chord_id=0;
  // int chord_counter_recoginiton_limit=10;
  // char char1to8[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
  for(int i=0; i<keypins_size;i++)
    {
      if (gpio_get_level(keypins[i])==0)
	{
	  // bt_write(get_keycode(char1to8[i],config.locale,&keycode_modifier,&keycode_deadkey_first));
	  // vTaskDelay(50 / portTICK_PERIOD_MS);
	  chord_id = (chord_id | (1ULL<<i));
	}
    }
  return chord_id;
}

const static uint16_t arrow_modus_chords[8][2]={
  // {chord_id,keycode},
  {1<<(1-1),KEY_BACKSPACE},
  {1<<(2-1),KEY_UP},
  {1<<(3-1),KEY_SPACE},
  {1<<(4-1),KEY_LEFT},
  {1<<(5-1),KEY_DOWN},
  {1<<(6-1),KEY_RIGHT},
  {1<<(7-1),KEY_ESC},
  {1<<(8-1),KEY_ENTER}
};
static int arrow_modus_chords_size = (sizeof(arrow_modus_chords)/sizeof(arrow_modus_chords[0]));
void arrow_modus(uint16_t chord_id)
{
  switch (chord_id)
    {
    case (1<<(5-1) | 1<<(7-1)):
      { // 57 change modus
	modi=get_modi(chord_id);
	while (!chord_actual()==0) // wait for key release
	  {
	    vTaskDelay(50 / portTICK_PERIOD_MS);
	  }
	chord_id = 0;
	break;
      }
    default:{
      break;
    }
    }
  if (!(chord_id == chord_id_prev))
    //ESP_LOGI(DEBUG_TAG,"arrow_mode, change detected");
    {
      keyboard_command_t keyboardCmd;
      for(int i=0; i<arrow_modus_chords_size;i++){
	if ((arrow_modus_chords[i][0] & chord_id) && !(arrow_modus_chords[i][0] & chord_id_prev))
	  // chord is pressed and it was not pressed before
	  {
	    keyboardCmd.keycode = (0xFF & arrow_modus_chords[i][1]) | (keycode_modifier<<8);
	    keyboardCmd.type = PRESS;
	    xQueueSend(keyboard_q,(void *)&keyboardCmd, (TickType_t) 0);
	  }
	if (!(arrow_modus_chords[i][0] & chord_id) && (arrow_modus_chords[i][0] & chord_id_prev))
	  // chord is not pressed and it was pressed before
	  {
	    keyboardCmd.keycode = ( 0xFF & arrow_modus_chords[i][1]) | (keycode_modifier<<8);
	    keyboardCmd.type = RELEASE;
	    xQueueSend(keyboard_q,(void *)&keyboardCmd, (TickType_t) 0);
	  }
      }
    }
  chord_id_prev=chord_id;
}

const static uint16_t wasd_modus_chords[8][2]={
  // {chord_id,keycode}, 
  {1<<(1-1),KEY_Q},
  {1<<(2-1),KEY_W},
  {1<<(3-1),KEY_E},
  {1<<(4-1),KEY_A},
  {1<<(5-1),KEY_S},
  {1<<(6-1),KEY_D},
  {1<<(7-1),KEY_ESC},
  {1<<(8-1),KEY_SPACE}
};
static int wasd_modus_chords_size = (sizeof(wasd_modus_chords)/sizeof(wasd_modus_chords[0]));

void wasd_modus(uint16_t chord_id)
{
  switch (chord_id)
    {
    case (1<<(5-1) | 1<<(7-1)):
      { // 57 change modus
	modi=get_modi(chord_id);
	while (!chord_actual()==0) // wait for key release
	  {
	    vTaskDelay(50 / portTICK_PERIOD_MS);
	  }
	chord_id=0;
	break;
      }
    default:{
      break;
    }
    }
  if (!(chord_id == chord_id_prev))
    {
      keyboard_command_t keyboardCmd;
      for(int i=0; i<wasd_modus_chords_size;i++){
	if ((wasd_modus_chords[i][0] & chord_id) && !(wasd_modus_chords[i][0] & chord_id_prev))
	  // chord is pressed and it was not pressed before
	  {
	    keyboardCmd.keycode = ( 0xFF & wasd_modus_chords[i][1]) | (keycode_modifier<<8);
	    keyboardCmd.type = PRESS;
	    xQueueSend(keyboard_q,(void *)&keyboardCmd, (TickType_t) 0);	    
	  }
	if (!(wasd_modus_chords[i][0] & chord_id) && (wasd_modus_chords[i][0] & chord_id_prev))
	  // chord is not pressed and it was pressed before
	  {
	    keyboardCmd.keycode = ( 0xFF & wasd_modus_chords[i][1]) | (keycode_modifier<<8);
	    keyboardCmd.type = RELEASE;
	    xQueueSend(keyboard_q,(void *)&keyboardCmd, (TickType_t) 0);	    
	  }
      }
    }
  chord_id_prev=chord_id;
}

void chord_id_keycode_bt(uint16_t chord_id)
{
  keycode_modifier = get_sticky_modifier(sticky_modifier);
  modi=get_modi(chord_id);
  switch(modi){ //support for more then two modi
  case 0:{ // default modus
    if(chord_id<256){ // keys only, no joystick
      if(!chord_id_keycode[chord_id][2] == 0)
	{ // deadkey first
	  uint16_t keycode_modifier_tmp = keycode_modifier;
	  keycode_modifier = chord_id_keycode[chord_id][3];
	  bt_write(chord_id_keycode[chord_id][2], keycode_modifier);
	  keycode_modifier = keycode_modifier_tmp;
	}
      keycode_modifier = (uint8_t) (keycode_modifier | chord_id_keycode[chord_id][1]);
      ESP_LOGI(DEBUG_TAG,"keycode_modifier in chord_id_keycode_bt: 0x%X", keycode_modifier);
      bt_write(chord_id_keycode[chord_id][0], keycode_modifier);
    } else { // joystick, chord_id>256
      joystick(chord_id);
    }
    break;
  }
  case 1:{ // wasd modus
    //wasd_modus(chord_id);
    break;
  }
  }
  set_sticky_modifier(chord_id);
  //keycode_modifier=0;
}


// uint16_t prev_chord_id
// boolean chord_was_greater_then_prev_chord
uint16_t chord_handler()
{
  uint16_t chord_id = chord_actual();
  if (chord_id < chord_id_prev)
    {// less keys in chord then in previous chord
      if (chord_was_greater_then_chord_prev)
	{// there was chord composition
	  chord_was_greater_then_chord_prev=false; //end of chord composition, start of chord decomposition
	  return chord_id_prev;
	}else
	{// chord decomposition
	  chord_id_prev=chord_id;
	}
    } else
    {
      if (chord_id > chord_id_prev)
	{// more keys in chord then in previous chord
	  chord_was_greater_then_chord_prev=true; //start of chord composition, end of chord decomposition
	  chord_id_prev=chord_id;
	}
    }
  return 0;
}

/* void update_config() */
/* { */
/*     nvs_handle my_handle; */
/*     esp_err_t err = nvs_open("fabi_c", NVS_READWRITE, &my_handle); */
/*     if(err != ESP_OK) ESP_LOGE("MAIN","error opening NVS"); */
/*     err = nvs_set_u8(my_handle, "btname_i", config.bt_device_name_index); */
/*     if(err != ESP_OK) ESP_LOGE("MAIN","error saving NVS - bt name"); */
/*     err = nvs_set_u8(my_handle, "locale", config.locale); */
/*     if(err != ESP_OK) ESP_LOGE("MAIN","error saving NVS - locale"); */
/*     printf("Committing updates in NVS ... "); */
/*     err = nvs_commit(my_handle); */
/*     printf((err != ESP_OK) ? "Failed!\n" : "Done\n"); */
/*     nvs_close(my_handle); */
/* } */

//gpio_input_pullup
void gpio_setup()
{
  //gpio_key_1, gpio_key_2, gpio_key_3, gpio_key_4, gpio_key_5, gpio_key_6, gpio_key_7, gpio_key_8
  uint64_t GPIO_INPUT_PIN_SEL=0;
  
  /* printf("(sizeof(pin1to8)/sizeof(pin1to8[0]))=%d\n",pin1to8size); */
  for(int i=0; i<keypins_size;i++)
    {
      /* printf("pin1to8[%d]=%d\n",i,pin1to8[i]); */
      GPIO_INPUT_PIN_SEL = (GPIO_INPUT_PIN_SEL | (1ULL<<keypins[i]));
      /* printf("GPIO_INPUT_PIN_SEL=%" PRIu64 "\n",GPIO_INPUT_PIN_SEL); */
    }
  gpio_config_t io_conf;
  io_conf.intr_type = GPIO_INTR_DISABLE;
    //GPIO_PIN_INTR_DISABLE;
  io_conf.mode = GPIO_MODE_INPUT;
  io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;
  //disable pull-down mode
  io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
  //enable pull-up mode
  io_conf.pull_up_en = GPIO_PULLUP_ENABLE;
  gpio_config(&io_conf);
  /* printf("GPIO_INPUT_PIN_SEL=%" PRIu64 "\n",GPIO_INPUT_PIN_SEL); */
}

void oskar_task(void *pvParameter)
{
  uint16_t chord_id=0;
  ESP_LOGI(OSKAR_TAG,"Oskar task started");
  // Initialize GPIO pins
  gpio_setup();

  while (1)
    {
      switch (modi)
	{
	case 1: // arrows, no chords
	  {
	    chord_id = chord_actual();
	    arrow_modus(chord_id);
	    vTaskDelay(100 / portTICK_PERIOD_MS);
	    break;
	  }
	case 2: // wasd, no chords
	  chord_id = chord_actual();
	  wasd_modus(chord_id);
	  vTaskDelay(100 / portTICK_PERIOD_MS);
	  break;
	default: // chord
	  {
	    chord_id = chord_handler();
	    keycode_modifier=0;
	    if (chord_id>0)
	      {
		ESP_LOGI(DEBUG_TAG, "chord: " BYTE_TO_BINARY_PATTERN " " BYTE_TO_BINARY_PATTERN " %d", BYTE_TO_BINARY(chord_id>>8), BYTE_TO_BINARY(chord_id), 0xFF & chord_id_keycode[chord_id][0]);
		/* ESP_LOGI(DEBUG_TAG,"unicode: 0x%X",chord_id_char[chord_id]); */
		/* ESP_LOGI(DEBUG_TAG,"unicode_to_keycode: 0x%X", unicode_to_keycode(chord_id_char[chord_id], config.locale)); */
		/* ESP_LOGI(DEBUG_TAG,"get_keycode: 0x%X", get_keycode(chord_id_char[chord_id],config.locale,&keycode_modifier,&keycode_deadkey_first)); */
		ESP_LOGI(DEBUG_TAG, "keycode_modifier: 0x%X", chord_id_keycode[chord_id][1]);
		ESP_LOGI(DEBUG_TAG,"keycode_deadkey: 0x%X", chord_id_keycode[chord_id][2]);
		ESP_LOGI(DEBUG_TAG,"keycode_deadkey_modifier: 0x%X", chord_id_keycode[chord_id][3]);
		/* uint8_t keycode_ = get_keycode(chord_id_char[chord_id],config.locale,&keycode_modifier,&keycode_deadkey_first); */
		/* if(keycode_deadkey_first>0) */
		/*   { */
		/* 	uint8_t keycode_modifier_tmp = keycode_modifier; */
		/* 	keycode_modifier = 0; */
		/* 	bt_write(keycode_deadkey_first); */
		/* 	keycode_modifier = keycode_modifier_tmp; */
		/*   }	     */
		/* bt_write(keycode_); */
		/* /\* bt_write(unicode_to_keycode(chord_id_char[chord_id],config.locale)); *\/ */
		chord_id_keycode_bt(chord_id);
	      }
	    vTaskDelay(50 / portTICK_PERIOD_MS);
	  }
	}
    }
}

extern "C" void app_main()
{
    esp_err_t ret;

    // Initialize NVS.
    ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK( ret );

    // Read config
    nvs_handle my_handle;
	ESP_LOGI("MAIN","loading configuration from NVS");
    ret = nvs_open("config_c", NVS_READWRITE, &my_handle);
    if(ret != ESP_OK) ESP_LOGE("MAIN","error opening NVS");
    size_t available_size = MAX_BT_DEVICENAME_LENGTH;
    strcpy(config.bt_device_name, GATTS_TAG);
    nvs_get_str (my_handle, "btname", config.bt_device_name, &available_size);
    if(ret != ESP_OK) 
    {
        ESP_LOGE("MAIN","error reading NVS - bt name, setting to default");
        strcpy(config.bt_device_name, GATTS_TAG);
    } else ESP_LOGI("MAIN","bt device name is: %s",config.bt_device_name);

    ret = nvs_get_u8(my_handle, "locale", &config.locale);
    if(ret != ESP_OK || config.locale >= LAYOUT_MAX) 
    {
        ESP_LOGE("MAIN","error reading NVS - locale, setting to US_INTERNATIONAL");
        config.locale = LAYOUT_US_INTERNATIONAL;
    } else ESP_LOGI("MAIN","locale code is : %d",config.locale);
    nvs_close(my_handle);

    // TBD: apply country code
    // load HID country code for locale before initialising HID
    // hidd_set_countrycode(get_hid_country_code(config.locale));


    //activate mouse & keyboard BT stack (joystick is not working yet)
    HID_kbdmousejoystick_init(1,1,0,0,config.bt_device_name);
    ESP_LOGI("HIDD","MAIN finished...");
    
    esp_log_level_set("*", ESP_LOG_INFO); 
  
    xTaskCreate(&oskar_task, "oskar", 4096, NULL, configMAX_PRIORITIES, NULL);
    //xTaskCreate(&blink_task, "blink", 4096, NULL, configMAX_PRIORITIES, NULL);
}
